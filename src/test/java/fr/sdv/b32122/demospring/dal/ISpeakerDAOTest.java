package fr.sdv.b32122.demospring.dal;

import fr.sdv.b32122.demospring.bo.Speaker;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ISpeakerDAOTest {
	
	@Autowired
	private ISpeakerDAO dao;
	
	@Test
	void testSave() {
		//A -> Arrangement
		Speaker speaker = new Speaker( "Séga S", "@ssy", "ssy@ssy.org" );
		//A -> Acting
		Speaker savedSpeaker = dao.save( speaker );
		//A -> Assert
		assertEquals( savedSpeaker.getTwitter(), speaker.getTwitter() );
	}
	
	@Test
	void testFindByTwitter() {
		// A -> Arrangement
		
		//A -> Acting
		Speaker speaker = dao.findByTwitter( "@ssy" );
		
		//A -> Assert
		assertEquals( 1, speaker.getId() );
	}
}