package fr.sdv.b32122.demospring.bo;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "T_SPEAKER")
public class Speaker implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	@Column(unique = true, length = 50)
	private String twitter;
	@Column(unique = true, length = 100)
	private String email;
	
	public Speaker() {}
	
	public Speaker( String name, String twitter, String email ) {
		this.name = name;
		this.twitter = twitter;
		this.email = email;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId( Long id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getTwitter() {
		return twitter;
	}
	
	public void setTwitter( String twitter ) {
		this.twitter = twitter;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Speaker{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", twitter='" ).append( twitter ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
