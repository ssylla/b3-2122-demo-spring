package fr.sdv.b32122.demospring.dal;

import fr.sdv.b32122.demospring.bo.Speaker;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(path = "my-speakers", collectionResourceRel = "speakers-list")
public interface ISpeakerDAO extends CrudRepository<Speaker, Long> {

	@RestResource(path = "twitter")
	Speaker findByTwitter(@Param( "id" ) String twitter );
	
}
