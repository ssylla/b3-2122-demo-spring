package fr.sdv.b32122.demospring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication
public class DemoSpringApplication {
	
	public static void main( String[] args ) {
		SpringApplication.run( DemoSpringApplication.class, args );
	}
	
	@Configuration
	static class WebSecurityConfig extends WebSecurityConfigurerAdapter {
		@Autowired
		public void configureGlobal( AuthenticationManagerBuilder auth ) throws Exception {
			auth.
						inMemoryAuthentication()
				.withUser( "user" ).password( "{noop}user" ).roles( "USER" ).and()
				.withUser( "admin" ).password( "{noop}admin" ).roles( "USER", "ADMIN" );
		}
		
		@Override
		protected void configure( HttpSecurity http ) throws Exception {
			http
					.authorizeRequests().antMatchers( "/**" ).hasAnyRole( "ADMIN", "USER" ).and()
					.httpBasic().and()
					.csrf().disable();
		}
	}
}
